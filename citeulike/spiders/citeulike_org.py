# -*- coding: utf-8 -*-
import scrapy
from citeulike.items import CiteulikeItem
from citeulike.items import CiteulikeConnectItem
import datetime
import codecs
import re


class CiteulikeOrgSpider(scrapy.Spider):
    name = "citeulike.org"
    allowed_domains = ["citeulike.org"]
    start_urls = (
        'http://www.citeulike.org/login',
    )
    users = {}
    connOut = open('connect.csv', 'w')
    tagsOut = open('tags.csv', 'w')
    flagsOut = open('flag.csv', 'w')
    usersList = open('allusers.csv')
    paperOut= codecs.open('paper.csv', 'w', 'utf8')
    lastId = 16556
    oldDate = datetime.date(2015, 3, 1)
    limitBookmarks = 250
    linksOnPage = 50
    tagPaperIds = []
    new_users = {}
        
    def parse(self, response):
        for row in self.usersList:
            self.users[row.split(',')[0]] = row.split(',')[1].rstrip()
        self.connOut.write('"username","userId","connectedUserId","username","flag"\n')
        self.tagsOut.write('"userId","paperId","tag"\n')
        self.flagsOut.write('"userId","paperId","flag"\n')
        self.paperOut.write('"paperId","title","authoList","edited","ident","source","date","year","volume",'
                            '"editNo","startPage","endPage","abstract","urls"\n')
        rq = scrapy.FormRequest.from_response(response, formname='frm',
                                              formdata={'username': 'fx', 'password': 'password'},
                                              callback=self.after_login)
        return rq
    
    def after_login(self, response):
        for user_id in self.users:
            rq = scrapy.Request('http://www.citeulike.org/connections/' + self.users[user_id],
                                callback=self.get_connections)
            rq.meta['user'] = self.users[user_id]
            rq.meta['user_id'] = user_id
            yield rq
        return
    
    def get_connections(self, response):
        if len(response.xpath('//div[@class="contentonly no-top-border"]/table/tr')) == 0:
            self.connOut.write('"' + response.meta['user'] + '","' + response.meta['userId'] + '","0","","0"\n')
        for row in response.xpath('//div[@class="contentonly no-top-border"]/table/tr'):
            name = ' '.join(row.xpath('.//td[2]/a/text()').extract())
            if name not in self.new_users:
                self.lastId += 1
                new_id = self.lastId
                self.new_users[name] = new_id
            else:
                new_id = self.new_users[name]
            flag = ' '.join(row.xpath('.//td[1]/text()').extract()).replace('from', '1').replace('to', '2')
            date = ' '.join(row.xpath('.//td[3]/text()').extract())
            dt = datetime.datetime.strptime(date, "%Y-%m-%d").date()
            if self.oldDate < dt:
                continue
            self.connOut.write('"' + response.meta['user'] + '","' + response.meta['userId'] + '","' + str(new_id) +
                               '","' + name + '","' + flag + '"\n')
            rq = scrapy.Request('http://www.citeulike.org/user/' + name, callback = self.get_publications)
            rq.meta['user'] = name
            rq.meta['userId'] = new_id
            rq.meta['cnt'] = 0
            rq.meta['flag'] = '5'
            yield rq
            rq = scrapy.Request('http://www.citeulike.org/user/' + name + '/publications',
                                callback=self.get_publications)
            rq.meta['user'] = name
            rq.meta['userId'] = new_id
            rq.meta['cnt'] = 0
            rq.meta['flag'] = '1'
            yield rq
        return
    
    def get_publications(self, response):
        cnt = response.meta['cnt']
        flag = response.meta['flag']
        for row in response.xpath('//table[@id="list_table"]/tr'):
            if cnt >= self.limitBookmarks:
                break
            link = ' '.join(row.xpath('.//h2/a/@href').extract())
            itm = {}
            itm['title'] = ' '.join(row.xpath('.//h2//text()').extract()).replace(u'\u2714  ', '').replace('"', "'")
            itm['paperId'] = ''.join(row.xpath('.//h2/a/@href').extract()).split('/')[4]
            itm['authorList'] = '; '.join(row.xpath('.//div[@class="vague"]/a[@class="author"]/text()').extract())
            itm['pubYear'] = ' '.join(' '.join(row.xpath('.//div[contains(@class,"vague posts")]/text()'
                                                         '').extract()).split()).replace(' /', '')[-19:]
            dt = datetime.datetime.strptime(itm['pubYear'], "%Y-%m-%d %H:%M:%S").date()
            if self.oldDate < dt:
                continue
            if itm['paperId'] not in self.tagPaperIds:
                self.tagPaperIds.append(itm['paperId'])
                rq = scrapy.Request(response.urljoin(link), callback=self.get_pub)
                rq.meta['itm'] = itm
                yield rq
                for tag in row.xpath('.//span[@class="taglist"]/a'):
                    tag = ' '.join(tag.xpath('./text()').extract())
                    self.tagsOut.write('"' + str(response.meta['userId']) + '","' + str(itm['paperId']) + '","' +
                                       tag + '"\n')
            self.flagsOut.write('"' + str(response.meta['userId']) + '","' + str(itm['paperId']) + '","' + flag + '"\n')
            cnt += 1
        for nextpage in response.xpath('//span[contains(text(),"Result page")]/b/a[text()="Next"]/@href').extract():
            rq = scrapy.Request(response.urljoin(nextpage), callback=self.get_publications)
            rq.meta['userId'] = response.meta['userId']
            rq.meta['cnt'] = cnt
            rq.meta['flag'] = flag
            if cnt < self.limitBookmarks:
                yield rq
            else:
                print 'LIMIT FOR ' + response.url
        return
    
    def get_pub(self, response):
        ident = source = volume = editNo = start_page = end_page = year = ''
        abstract = ' '.join(response.xpath('//div[@id="abstract-body"]/blockquote/p/text()').extract())
        edited = '; '.join(response.xpath('//h2/a[@class="author"]/text()').extract())
        situation = ' '.join(response.xpath('//div[@id="citation-plain"]//text()').extract()).replace('"', "'")
        pp = re.compile('.*pp. (\d+)-(\d+).*').match(situation)
        vl = re.compile('.*Vol. (\d+[ ]?-?[ ]?\d*).*').match(situation)
        no = re.compile('.*No. (\d+[ ]?-?[ ]?\d*).*').match(situation)
        doi = re.compile('.*doi:(\S*).*').match(situation)
        upd_sit = re.compile('(.*?)(Vol|pp|Key).*').match(situation)
        yr = re.compile('.*\(.*(20\d\d).*\).*').match(situation)
        dt = re.compile('.*(\(.*20\d\d.*\)).*').match(situation)
        if pp is not None:
            start_page = pp.group(1)
            end_page = pp.group(2)
        if vl is not None:
            volume = vl.group(1)
        if no is not None:
            editNo = no.group(1)
        if doi is not None:
            ident = doi.group(1)
        if yr is not None:
            year = yr.group(1)
        if upd_sit is not None:
            source = upd_sit.group(1)
            if dt is not None:
                date = dt.group(1)
                if len(source) - len(date) < 5:
                    source = ''
        urls = '","'.join(response.xpath('//span[@id="linkouts"]/a/@href').extract())
        itm = response.meta['itm']
        self.paperOut.write('"' + str(itm['paperId']) + '","' + itm['title'] + '","' + itm['authorList'] + '","' + 
                            edited + '","' + ident + '","' + source + '","' + itm['pubYear'] + '","' + year + '","' +
                            volume + '","' + editNo + '","' + start_page + '","' + end_page + '","' + abstract + '","' +
                            urls + '"\n')
        return

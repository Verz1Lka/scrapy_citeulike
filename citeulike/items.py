# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class CiteulikeItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    paperId = scrapy.Field()
    title = scrapy.Field()
    authorList = scrapy.Field()
    abstract = scrapy.Field()
    pubYear = scrapy.Field()
    
class CiteulikeConnectItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    userId = scrapy.Field()
    connectedUser = scrapy.Field()
    flag = scrapy.Field()
